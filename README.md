# GEOIP Lookup Service

## Architecture
- LookupService loads the GEOIP2 database once on startup.
- Executor asyncExecutor provides a separate thread pool for servicing lookup requests.
- AsyncService processes a list of lookup requests asynchronously, such that the duration of the job is just slightly more than the longest lookup operation.
- LookupService implements a Completable Future on getCountry, in order to enable asynchronous lookups.
- LookupController implements DeferredResult so that all calls are non-blocking.

## API Endpoints
- [GET] /country?ip4={valid.IP.Address} - translates a single IP address to LookupResponse.
- [POST] /countries with a JSON list in the request body - returns a list of LookupResponses.

## API Output Example
```
{
    "lookupResponseList": [
        {
            "ipv4Address": "111.111.111.111",
            "countryName": "Japan",
            "countryCode": "JP",
            "status": 200,
            "duration": 2
        },
        {
            "ipv4Address": "99.99.99.99",
            "countryName": "United States",
            "countryCode": "US",
            "status": 200,
            "duration": 3
        },
        {
            "ipv4Address": "999.999.999.999",
            "countryName": "Failed: 999.999.999.999 is not a valid address.",
            "countryCode": null,
            "status": 204,
            "duration": 2
        },
        {
            "ipv4Address": "214.45.56.78",
            "countryName": "United States",
            "countryCode": "US",
            "status": 200,
            "duration": 3
        },
        {
            "ipv4Address": "55.22.33.44",
            "countryName": "United States",
            "countryCode": "US",
            "status": 200,
            "duration": 10
        },
        {
            "ipv4Address": "222.11.99.33",
            "countryName": "Japan",
            "countryCode": "JP",
            "status": 200,
            "duration": 7
        },
        {
            "ipv4Address": "94.45.56.78",
            "countryName": "Ukraine",
            "countryCode": "UA",
            "status": 200,
            "duration": 8
        },
        {
            "ipv4Address": "101.22.33.44",
            "countryName": "China",
            "countryCode": "CN",
            "status": 200,
            "duration": 9
        },
        {
            "ipv4Address": "77.11.99.33",
            "countryName": "Germany",
            "countryCode": "DE",
            "status": 200,
            "duration": 15
        },
        {
            "ipv4Address": "39.45.56.78",
            "countryName": "Pakistan",
            "countryCode": "PK",
            "status": 200,
            "duration": 10
        },
        {
            "ipv4Address": "192.22.33.44",
            "countryName": "United States",
            "countryCode": "US",
            "status": 200,
            "duration": 12
        },
        {
            "ipv4Address": "23.11.99.33",
            "countryName": "United States",
            "countryCode": "US",
            "status": 200,
            "duration": 18
        },
        {
            "ipv4Address": "130.45.56.78",
            "countryName": "United States",
            "countryCode": "US",
            "status": 200,
            "duration": 14
        },
        {
            "ipv4Address": "188.22.33.44",
            "countryName": "Austria",
            "countryCode": "AT",
            "status": 200,
            "duration": 21
        },
        {
            "ipv4Address": "78.11.99.33",
            "countryName": "Poland",
            "countryCode": "PL",
            "status": 200,
            "duration": 28
        }
    ],
    "duration": 30
}
```
