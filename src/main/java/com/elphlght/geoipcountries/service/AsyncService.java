package com.elphlght.geoipcountries.service;

import com.elphlght.geoipcountries.domain.LookupResponse;
import com.elphlght.geoipcountries.domain.LookupTask;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

@Slf4j
@Service
public class AsyncService {
    private final LookupService lookupService;

    @Autowired
    public AsyncService(LookupService lookupService){
        this.lookupService = lookupService;
    }

    public void execute(final LookupTask lookupTask){
        log.info("Started task with {} addresses", lookupTask.getIps().size());
        lookupTask.start();

        for(int i = 0; i < lookupTask.getIps().size(); i++) {
            final long time = System.currentTimeMillis();
            final int index = i;
            String ipAddress = lookupTask.getIps().get(i);

            CompletableFuture<LookupResponse> lookupResponse = lookupService.getCountry(ipAddress)
                    .whenComplete(((lookupResponse1, throwable) -> {
                        if(lookupResponse1.getStatus() == 200) {
                            lookupTask.success(index, time, lookupResponse1);
                        } else {
                            Exception ex = new Exception(String.format("%s is not a valid address.",ipAddress));
                            lookupTask.fail(index, time, ipAddress, ex);
                        }
                    }));
        }
    }
}
