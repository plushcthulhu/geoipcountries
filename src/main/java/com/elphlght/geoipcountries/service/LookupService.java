package com.elphlght.geoipcountries.service;

import com.elphlght.geoipcountries.domain.LookupResponse;
import com.maxmind.db.Reader;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CountryResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.CompletableFuture;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Service
public class LookupService {

    private static final String IPADDRESS_PATTERN =
            "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)";

    // private static Logger LOGGER = LoggerFactory.getLogger(LookupService.class);

    private static DatabaseReader databaseReader = null;

    private final ResourceLoader resourceLoader;

    @Autowired
    public LookupService(final ResourceLoader resourceLoader){
        this.resourceLoader = resourceLoader;
    }

    @PostConstruct
    public void init(){
        try {
            Resource resource = resourceLoader.getResource("classpath:geoip/GeoLite2-Country.mmdb");

            File dbAsFile = resource.getFile();

            // Initialize the reader
            databaseReader = new DatabaseReader
                    .Builder(dbAsFile)
                    .fileMode(Reader.FileMode.MEMORY)
                    .build();

            log.info("LookupService: Database was loaded successfully.");

        } catch (IOException | NullPointerException e) {
            log.error("Database reader could not be initialized. ", e);
        }
    }

    @Async
    public CompletableFuture<LookupResponse> getCountry(String ipAddress){
        try{
            log.info("Looking up {}",ipAddress);
            String validIP = validIPAddress(ipAddress);
            if(!validIP.equals("0.0.0.0")) {
                InetAddress inetAddress = InetAddress.getByName(validIP);
                CountryResponse response = databaseReader.country(inetAddress);
                LookupResponse result = new LookupResponse(ipAddress,response.getCountry().getName(),
                        response.getCountry().getIsoCode(),200,0);
                return CompletableFuture.completedFuture(result);
            } else {
                LookupResponse failed = new LookupResponse(ipAddress,"Unknown","Unknown",204,0);
                return CompletableFuture.completedFuture(failed);
            }
        } catch (UnknownHostException e) {
            log.error(String.format("Unknown Host: %s",ipAddress));
            // e.printStackTrace();
        } catch (IOException e) {
            log.error("Database file could not be read.");
            // e.printStackTrace();
        } catch (GeoIp2Exception e) {
            log.error("Geo2IP threw an exception");
            // e.printStackTrace();
        }

        return null;
    }

    private String validIPAddress(String ipString){

        Pattern pattern = Pattern.compile(IPADDRESS_PATTERN);
        Matcher matcher = pattern.matcher(ipString);
        if (matcher.find()) {
            return matcher.group();
        } else{
            return "0.0.0.0";
        }
    }

    @PreDestroy
    public void preDestroy() {
        if (databaseReader != null) {
            try {
                databaseReader.close();
            } catch (IOException e) {
                log.error("Failed to close the reader.");
            }
        }
    }


}
