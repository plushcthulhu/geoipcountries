package com.elphlght.geoipcountries.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LookupResponse {
    private String ipv4Address;
    private String countryName;
    private String countryCode;
    private int status;
    private int duration;
}
