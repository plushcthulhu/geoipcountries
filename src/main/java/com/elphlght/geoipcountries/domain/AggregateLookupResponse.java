package com.elphlght.geoipcountries.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AggregateLookupResponse {
    private List<LookupResponse> lookupResponseList;
    private int duration;
}
