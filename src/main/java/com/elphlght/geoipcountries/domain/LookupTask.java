package com.elphlght.geoipcountries.domain;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.async.DeferredResult;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.Collections.unmodifiableList;
import static java.util.stream.Collectors.toList;

@Slf4j
public class LookupTask {
    private final AtomicInteger counter;
    private final DeferredResult<ResponseEntity<AggregateLookupResponse>> result;
    private final List<String> ips;
    private final List<LookupResponse> responses;
    private long startTime;

    public LookupTask(final DeferredResult<ResponseEntity<AggregateLookupResponse>> result, final List<String> ips) {
        this.counter = new AtomicInteger(ips.size());
        this.ips = ips;
        this.result = result;
        this.responses = ips.stream().map(s -> new LookupResponse()).collect(toList());
    }

    public List<String> getIps() { return unmodifiableList(ips);}


    public void fail(int index, long time, String ip, Exception e) {
        responses.get(index).setIpv4Address(ip);
        responses.get(index).setStatus(204);
        responses.get(index).setCountryName("Failed: " + e.getMessage());
        responses.get(index).setDuration((int)(System.currentTimeMillis() - time));

        checkDone();
    }

    public void success(int index, long time, LookupResponse response) {
        responses.get(index).setStatus(response.getStatus());
        responses.get(index).setIpv4Address(response.getIpv4Address());
        responses.get(index).setCountryCode(response.getCountryCode());
        responses.get(index).setCountryName(response.getCountryName());
        responses.get(index).setDuration((int)(System.currentTimeMillis() - time));

        checkDone();
    }

    public void start() {
        startTime = System.currentTimeMillis();
    }

    private void checkDone() {
        synchronized (counter) {
            if (counter.decrementAndGet() == 0) {
                AggregateLookupResponse response = new AggregateLookupResponse(responses, (int)(System.currentTimeMillis() - startTime));
                result.setResult(ResponseEntity.ok(response));
                log.info("Finished task in {} ms", response.getDuration());
            }
        }
    }

}
