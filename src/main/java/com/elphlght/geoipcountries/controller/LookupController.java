package com.elphlght.geoipcountries.controller;

import com.elphlght.geoipcountries.domain.AggregateLookupResponse;
import com.elphlght.geoipcountries.domain.LookupTask;
import com.elphlght.geoipcountries.service.AsyncService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.ArrayList;
import java.util.List;

@RestController

public class LookupController {

    private final AsyncService service;

    @Autowired
    public LookupController(final AsyncService asyncService){
        this.service = asyncService;
    }

    @RequestMapping(value = "/country",method = RequestMethod.GET)
    public DeferredResult<ResponseEntity<AggregateLookupResponse>> lookupCountry(@RequestParam(name = "ip4")String ip4){
        List<String> addresses = new ArrayList<>();
        addresses.add(ip4);
        DeferredResult<ResponseEntity<AggregateLookupResponse>> result = new DeferredResult<>();
        LookupTask task = new LookupTask(result, addresses);
        service.execute(task);
        return result;

    }

    @RequestMapping(value = "/countries", method = RequestMethod.POST)
    @ResponseBody
    public DeferredResult<ResponseEntity<AggregateLookupResponse>> call(@RequestBody final List<String> request) {

        DeferredResult<ResponseEntity<AggregateLookupResponse>> result = new DeferredResult<>();
        LookupTask task = new LookupTask(result, request);
        service.execute(task);

        return result;
    }

}
