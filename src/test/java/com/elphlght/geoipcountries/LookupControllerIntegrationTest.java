package com.elphlght.geoipcountries;

import com.elphlght.geoipcountries.domain.LookupResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class LookupControllerIntegrationTest {
    private static final ObjectMapper MAPPER = new ObjectMapper();

    @Autowired
    private WebApplicationContext wac;

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(WireMockConfiguration.options().dynamicPort());

    private String baseUrl;
    private MockMvc mockMvc;

    private final String[] IP_ADDRESSES = {
            "111.111.111.111",
            "99.99.99.99",
            "999.999.999.999",
            "214.45.56.78",
            "11.22.33.44",
            "23.11.99.33",
            "214.45.56.78",
            "141.22.33.44"
    };


    @Before
    public void setup() throws Exception {
        mockMvc = webAppContextSetup(this.wac).build();
        baseUrl = "http://localhost:" + wireMockRule.port();

        stubServices();
    }

    private void stubServices() throws Exception {
        //Total delay = 8 * 250 = 2000ms.
        stubService(IP_ADDRESSES[0], new LookupResponse(IP_ADDRESSES[0],"Japan","JP",200,0));
        stubService(IP_ADDRESSES[1], new LookupResponse(IP_ADDRESSES[1],"United States","US",200,0));
        stubService(IP_ADDRESSES[2], new LookupResponse(IP_ADDRESSES[2],"Failed: 999.999.999.999 in not valid address.","",204,0));
        stubService(IP_ADDRESSES[3], new LookupResponse(IP_ADDRESSES[3],"United States","US",200,0));
        stubService(IP_ADDRESSES[4], new LookupResponse(IP_ADDRESSES[4],"France","FR",200,0));
        stubService(IP_ADDRESSES[5], new LookupResponse(IP_ADDRESSES[5],"United States","US",200,0));
        stubService(IP_ADDRESSES[6], new LookupResponse(IP_ADDRESSES[6],"United States","US",200,0));
        stubService(IP_ADDRESSES[7], new LookupResponse(IP_ADDRESSES[7],"Germany","DE",200,0));
    }

    private void stubService(String route, Object value) throws Exception {
        stubFor(get(urlEqualTo(route))
                .willReturn(aResponse()
                        .withFixedDelay(250)
                        .withHeader("Content-Type", "application/json")
                        .withBody(MAPPER.writeValueAsBytes(value))));
    }

    @Test
    public void asyncFlow() throws Exception {
        List<String> request = Arrays.stream(IP_ADDRESSES).collect(Collectors.toList());

        MvcResult resultActions = mockMvc.perform(post("/countries")
                .contentType(MediaType.APPLICATION_JSON)
                .content(MAPPER.writeValueAsString(request)))
                .andExpect(request().asyncStarted())
                .andReturn();

        mockMvc.perform(asyncDispatch(resultActions))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.lookupResponseList[0].status").value(200))
                .andExpect(jsonPath("$.lookupResponseList[0].duration").isNumber())
                .andExpect(jsonPath("$.lookupResponseList[0].countryName").value("Japan"))
                .andExpect(jsonPath("$.lookupResponseList[1].countryCode").value("US"))
                .andExpect(jsonPath("$.lookupResponseList[2].countryName").value("Failed: 999.999.999.999 is not a valid address."))
                .andExpect(jsonPath("$.lookupResponseList[3].countryName").value("United States"))
                .andExpect(jsonPath("$.lookupResponseList[4].countryCode").value("FR"))
                .andExpect(jsonPath("$.lookupResponseList[5].ipv4Address").value("23.11.99.33"))
                .andExpect(jsonPath("$.lookupResponseList[6].status").value(200))
                .andExpect(jsonPath("$.lookupResponseList[7].countryName").value("Germany"))
                .andExpect(jsonPath("$.duration", Matchers.lessThan(750))); //Should be around 500ms.
    }

}
